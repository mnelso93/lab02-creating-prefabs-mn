﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//Michael Nelson, Aug. 28th, create prefabs in the project panel through a menu item
public class Prefab_Maker : MonoBehaviour {

    //Generate the new prefab
    [MenuItem("Project Tools/Create Prefab")]
    public static void CreatePrefab()
    {
        //Geather each of the selected gameObjects
        GameObject[] selectedObjects = Selection.gameObjects;

        //get the name and location to store the object and store it there
        foreach (GameObject go in selectedObjects)
        {
            //The name and desired storage location
            string name = go.name;
            string assetPath = EditorUtility.SaveFilePanelInProject("Save prefab to folder", go.name, "prefab", "");

            //If the asset already exists make sure they want to overwrite it
            if (AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
            {
                
                if (EditorUtility.DisplayDialog("Caution", name + " already exists. Do you want to overwrite?", "Yes", "No"))
                {
                    //Create the new prefab
                    CreateNew(go, assetPath);
                }
            }
            else
            {
                //Create the new prefab
                CreateNew(go, assetPath);
            }
            //Debug.Log("Name: " + go.name + " Path: " + assetPath);
        }
    }

    public static void CreateNew(GameObject obj, string location)
    {
        //Make the prefab
        Object prefab = PrefabUtility.CreateEmptyPrefab(location);
        PrefabUtility.ReplacePrefab(obj, prefab);
        AssetDatabase.Refresh();

        //Destroy the old object
        DestroyImmediate(obj);

        //Instantiate the prefab in the old object's place
        GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
    }
}
